﻿using HW3.DAL.EF;
using HW3.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HW3.DAL.Repository
{
    public class UserRepository : IRepository<UserEntity>
    {
        private ProjectsContext _context;
        public UserRepository(ProjectsContext context)
        {
            _context = context;
        }
        public UserEntity Create(UserEntity item)
        {
            _context.Users.Add(item);
            return item;
        }

        public async Task Delete(int id)
        {
            var users = await _context.Users.ToListAsync();
            var user = users.Where(user => user.Id == id).FirstOrDefault();
            _context.Users.Remove(user);
        }

        public async Task<UserEntity> Get(int id)
        {
            var users = await _context.Users.ToListAsync();
            return users.Where(user => user.Id == id).FirstOrDefault();
        }

        public async Task<List<UserEntity>> GetAll()
        {
            var result = await _context.Users.ToListAsync();
            return result;
        }

        public async Task<UserEntity> Update(UserEntity item)
        {
            var entity = await _context.Users.FirstOrDefaultAsync(p => p.Id == item.Id);
            if (entity == null)
            {
                throw new Exception();
            }
            entity.FirstName = item.FirstName;
            entity.LastName = item.LastName;
            _context.Users.Update(entity);
            var users = await _context.Users.ToListAsync();
            return users.Where(team => team.Id == item.Id).FirstOrDefault();
        }
    }
}
