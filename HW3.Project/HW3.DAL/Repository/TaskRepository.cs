﻿using HW3.DAL.EF;
using HW3.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HW3.DAL.Repository
{
    public class TaskRepository : IRepository<TaskEntity>
    {
        private ProjectsContext _context;
        public TaskRepository(ProjectsContext context)
        {
            _context = context;
        }
        public TaskEntity Create(TaskEntity item)
        {
            _context.Tasks.Add(item);
            return item;
        }

        public async Task Delete(int id)
        {
            var tasks = await _context.Tasks.ToListAsync();
            var task = tasks.Where(task => task.Id == id).FirstOrDefault();
            _context.Tasks.Remove(task);
        }

        public async Task<TaskEntity> Get(int id)
        {
            var tasks = await _context.Tasks.ToListAsync();
            return tasks.Where(task => task.Id == id).FirstOrDefault();
        }

        public async Task<List<TaskEntity>> GetAll()
        {
            var result = await _context.Tasks.ToListAsync();
            return result;
        }

        public async Task<TaskEntity> Update(TaskEntity item)
        {
            var entity = await _context.Tasks.FirstOrDefaultAsync(p => p.Id == item.Id);
            if (entity == null)
            {
                throw new Exception();
            }
            entity.Name = item.Name;
            entity.Description = item.Description;
            _context.Tasks.Update(entity);
            var tasks = await _context.Tasks.ToListAsync();
            return tasks.Where(task => task.Id == item.Id).FirstOrDefault();
        }
    }
}