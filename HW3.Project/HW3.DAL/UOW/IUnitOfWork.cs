﻿using HW3.DAL.Entities;
using HW3.DAL.Repository;
using System.Threading.Tasks;

namespace HW3.DAL.UOW
{
    public interface IUnitOfWork
    {
        IRepository<ProjectEntity> Projects { get; }
        IRepository<UserEntity> Users { get; }
        IRepository<TaskEntity> Tasks { get; }
        IRepository<TeamEntity> Teams { get; }
        Task Save();
    }
}
