﻿using AutoMapper;
using Common.DTO.Task;
using HW3.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW3.BLL.MappingProfiles
{
    public class TaskCreatedProfile : Profile
    {
        public TaskCreatedProfile()
        {
            CreateMap<TaskCreatedDTO, TaskEntity>();
        }
    }
}
