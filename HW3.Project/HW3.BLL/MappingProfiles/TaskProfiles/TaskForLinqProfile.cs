﻿using AutoMapper;
using Common.DTO;
using HW3.BLL.ModelsForLINQ;

namespace HW3.BLL.MappingProfiles
{
    public class TaskForLinqProfile : Profile
    {
        public TaskForLinqProfile()
        {
            CreateMap<ProjectTask, TaskDTO>().ForMember(task => task.PerformerId, src => src.MapFrom(member => member.Performer.Id));
            CreateMap<TaskDTO, ProjectTask>();
        }
    }
}
