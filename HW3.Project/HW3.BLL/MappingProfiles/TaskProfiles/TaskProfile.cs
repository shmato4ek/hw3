﻿using AutoMapper;
using Common.DTO;
using HW3.DAL.Entities;

namespace HW3.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskEntity, TaskDTO>();
            CreateMap<TaskDTO, TaskEntity>();
        }
    }
}
