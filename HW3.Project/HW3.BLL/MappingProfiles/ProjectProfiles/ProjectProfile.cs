﻿using AutoMapper;
using Common.DTO;
using HW3.DAL.Entities;

namespace HW3.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectEntity, ProjectDTO>();
            CreateMap<ProjectDTO, ProjectEntity>();
        }
    }
}
