﻿using AutoMapper;
using Common.DTO;
using HW3.BLL.ModelsForLINQ;

namespace HW3.BLL.MappingProfiles
{
    public class ProjectLinqProfile : Profile
    {
        public ProjectLinqProfile()
        {
            CreateMap<Project, ProjectDTO>();
        }
    }
}
