﻿using AutoMapper;
using Common.DTO;
using HW3.BLL.Structs;

namespace HW3.BLL.MappingProfiles
{
    public class UserStructProfile : Profile
    {
        public UserStructProfile()
        {
            CreateMap<UserStruct, UserStructDTO>();
        }
    }
}
