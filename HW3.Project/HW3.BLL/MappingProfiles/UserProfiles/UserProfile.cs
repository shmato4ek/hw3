﻿using AutoMapper;
using Common.DTO;
using HW3.DAL.Entities;

namespace HW3.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserEntity, UserDTO>();
            CreateMap<UserDTO, UserEntity>();
        }
    }
}
