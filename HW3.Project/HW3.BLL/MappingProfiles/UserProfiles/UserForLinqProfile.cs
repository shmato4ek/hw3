﻿using AutoMapper;
using Common.DTO;
using HW3.BLL.ModelsForLINQ;

namespace HW3.BLL.MappingProfiles
{
    public class UserForLinqProfile : Profile
    {
        public UserForLinqProfile()
        {
            CreateMap<User, UserDTO>();
        }
    }
}
