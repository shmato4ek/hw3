﻿using AutoMapper;
using Common.DTO.User;
using HW3.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW3.BLL.MappingProfiles
{
    public class UserCreatedProfile : Profile
    {
        public UserCreatedProfile()
        {
            CreateMap<UserCreatedDTO, UserEntity>();
        }
    }
}
