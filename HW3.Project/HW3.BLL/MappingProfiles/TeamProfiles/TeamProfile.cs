﻿using AutoMapper;
using Common.DTO;
using HW3.DAL.Entities;

namespace HW3.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<TeamEntity, TeamDTO>();
            CreateMap<TeamDTO, TeamEntity>();
        }
    }
}
