﻿using AutoMapper;
using Common.DTO;
using HW3.BLL.Interfaces;
using HW3.BLL.Services.Abstract;
using HW3.DAL.UOW;
using System.Collections.Generic;
using HW3.DAL.Entities;
using System.Threading.Tasks;
using Common.DTO.User;

namespace HW3.BLL.Services
{
    public class UserService : BaseService, IService<UserDTO, UserCreatedDTO>
    {
        public UserService(IMapper mapper, IUnitOfWork uow) : base(mapper, uow) { }

        public async Task<List<UserDTO>> GetAll()
        {
            var result = await data.Users.GetAll();
            return _mapper.Map<List<UserDTO>>(result);
        }

        public async Task<UserDTO> Get(int id)
        {
            var user = await data.Users.Get(id);
            return _mapper.Map<UserDTO>(user);
        }

        public async Task<UserDTO> Add(UserCreatedDTO user)
        {
            var newUser = _mapper.Map<UserEntity>(user);
            var addedUser = data.Users.Create(newUser);
            await data.Save();
            return _mapper.Map<UserDTO>(addedUser);
        }

        public async Task<UserDTO> Update(UserDTO user)
        {
            var newUser = _mapper.Map<UserEntity>(user);
            var updatedUser = await data.Users.Update(newUser);
            await data.Save();
            return _mapper.Map<UserDTO>(updatedUser);
        }

        public async Task Delete(int id)
        {
            await data.Users.Delete(id);
            await data.Save();
        }
    }
}
