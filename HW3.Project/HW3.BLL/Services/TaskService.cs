﻿using AutoMapper;
using Common.DTO;
using HW3.BLL.Interfaces;
using HW3.BLL.Services.Abstract;
using HW3.DAL.UOW;
using System.Collections.Generic;
using HW3.DAL.Entities;
using System.Threading.Tasks;
using Common.DTO.Task;

namespace HW3.BLL.Services
{
    public class TaskService : BaseService, IService<TaskDTO, TaskCreatedDTO>
    {
        public TaskService(IMapper mapper, IUnitOfWork uow) : base(mapper, uow) { }

        public async Task<List<TaskDTO>> GetAll()
        {
            var result = await data.Tasks.GetAll();
            return _mapper.Map<List<TaskDTO>>(result);
        }

        public async Task<TaskDTO> Get(int id)
        {
            var task = await data.Tasks.Get(id);
            return _mapper.Map<TaskDTO>(task);
        }

        public async Task<TaskDTO> Add(TaskCreatedDTO task)
        {
            var newTask = _mapper.Map<TaskEntity>(task);
            var addedTask = data.Tasks.Create(newTask);
            await data.Save();
            return _mapper.Map<TaskDTO>(addedTask);
        }

        public async Task<TaskDTO> Update(TaskDTO task)
        {
            var newTask = _mapper.Map<TaskEntity>(task);
            var updatedTask = await data.Tasks.Update(newTask);
            return _mapper.Map<TaskDTO>(updatedTask);
        }

        public async Task Delete(int id)
        {
            await data.Tasks.Delete(id);
            await data.Save();
        }
    }
}
