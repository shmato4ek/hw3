﻿namespace Common.DTO
{
    public enum TaskStates
    {
        Canceled = 0,
        Started,
        Finished,
        Stopped
    }
}
