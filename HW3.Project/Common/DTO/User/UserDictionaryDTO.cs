﻿using System.Collections.Generic;

namespace Common.DTO
{
    public class UserDictionaryDTO
    {
        public UserDTO Key { get; set; }
        public List<TaskDTO> Value{ get; set; }
    }
}
