﻿using Common.DTO;
using Common.DTO.Project;
using Common.DTO.Task;
using Common.DTO.Team;
using Common.DTO.User;
using HW3.BLL.Structs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HW3.ConsoleProject
{
    class Program
    {
        static async Task Main(string[] args)
        {
            bool flag = true;
            using (HttpClient client = new HttpClient())
            {
                while (flag)
                {
                    Console.WriteLine("*********************************************************************************************************");
                    Console.WriteLine("\tWelcome to my console :)\n\tThere is list of all tasks:\n\n");
                    Console.WriteLine("1. Tasks.\n" +
                        "2. Projects.\n" +
                        "3. Teams.\n" +
                        "4. Users.\n" +
                        "5. LINQ\n" +
                        "0. Exit\n");
                    Console.Write("Write the number of task: ");
                    int answer1 = Int32.Parse(Console.ReadLine());
                    switch (answer1)
                    {
                        case 1:
                            Console.WriteLine("\n\n\tTasks:\n");
                            Console.WriteLine("1. Get all.\n" +
                                "2. Get task by id.\n" +
                                "3. Update task.\n" +
                                "4. Delete task.\n" +
                                "5. Add task.\n");
                            Console.Write(" Write number: ");
                            int answer2 = Int32.Parse(Console.ReadLine());
                            switch (answer2)
                            {
                                case 1:
                                    await GetAllTasks(client);
                                    break;
                                case 2:
                                    try
                                    {
                                        await GetTaskById(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 3:
                                    try
                                    {
                                        await UpdateTask(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 4:
                                    try
                                    {
                                        await DeleteTask(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 5:
                                    try
                                    {
                                        await AddNewTask(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2:
                            Console.WriteLine("\n\n\tProjects:\n");
                            Console.WriteLine("1. Get all.\n" +
                                "2. Get project by id.\n" +
                                "3. Update project.\n" +
                                "4. Delete project.\n" +
                                "5. Add project.\n");
                            Console.Write(" Write number: ");
                            int answer3 = Int32.Parse(Console.ReadLine());
                            switch (answer3)
                            {
                                case 1:
                                    await GetAllProjects(client);
                                    break;
                                case 2:
                                    try
                                    {
                                        await GetProjectById(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 3:
                                    try
                                    {
                                        await UpdateProject(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 4:
                                    try
                                    {
                                        await DeleteProject(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 5:
                                    try
                                    {
                                        await AddNewProject(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:
                            Console.WriteLine("\n\n\tTeasm:\n");
                            Console.WriteLine("1. Get all.\n" +
                                "2. Get team by id.\n" +
                                "3. Update team.\n" +
                                "4. Delete team.\n" +
                                "5. Add team.\n");
                            Console.Write(" Write number: ");
                            int answer4 = Int32.Parse(Console.ReadLine());
                            switch (answer4)
                            {
                                case 1:
                                    await GetAllTeams(client);
                                    break;
                                case 2:
                                    try
                                    {
                                        await GetTeamById(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 3:
                                    try
                                    {
                                        await UpdateTeam(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 4:
                                    try
                                    {
                                        await DeleteTeam(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 5:
                                    try
                                    {
                                        await AddNewTeam(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 4:
                            Console.WriteLine("1. Get all.\n" +
                                "2. Get user by id.\n" +
                                "3. Update user.\n" +
                                "4. Delete user.\n" +
                                "5. Add user.\n");
                            Console.Write(" Write number: ");
                            int answer5 = Int32.Parse(Console.ReadLine());
                            switch (answer5)
                            {
                                case 1:
                                    await GetAllUsers(client);
                                    break;
                                case 2:
                                    try
                                    {
                                        await GetUserById(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 3:
                                    try
                                    {
                                        await UpdateUser(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 4:
                                    try
                                    {
                                        await DeleteUser(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 5:
                                    try
                                    {
                                        await AddNewUser(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 5:
                            Console.WriteLine("\n\n\tLINQ\n\n");
                            Console.WriteLine("1. Ammount of tasks of concrete user`s project.\n" +
                                "2. List of concrete user`s tasks with length < 45 symbols.\n" +
                                "3. List of concrete user`s tasks that were completed in 2021.\n" +
                                "4. List of teams with 10 and more y.o. users, sorted by time of registration discending and grouped by teams.\n" +
                                "5. List of users sorted by first name wits tasks, sorted by length of name discending.\n" +
                                "6. Special struct#1.\n" +
                                "7. Special struct#2.\n" +
                                "0. Exit\n" +
                                "*********************************************************************************************************\n\n");
                            Console.Write("\tWrite the number of task: ");
                            int answer = Int32.Parse(Console.ReadLine());
                            switch (answer)
                            {
                                case 1:
                                    try
                                    {
                                        await AmountOfTasksOfProject(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    Console.WriteLine();
                                    break;
                                default:
                                    break;
                                case 2:
                                    try
                                    {
                                        await ConcreteUsersTask(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    Console.WriteLine();
                                    break;
                                case 3:
                                    try
                                    {
                                        await TasksCompletedIn2021(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    Console.WriteLine();
                                    break;
                                case 4:
                                    await TeamsWith10MoreUser(client);
                                    break;
                                case 5:
                                    await SortedListOfUsers(client);
                                    break;
                                case 6:
                                    try
                                    {
                                        await GetUsersInfo(client);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    Console.WriteLine();
                                    break;
                                case 7:
                                    await GetProjectsInfo(client);
                                    Console.WriteLine();
                                    break;
                            }
                            break;
                        case 0:
                            flag = false;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private static async Task GetAllTasks(HttpClient client)
        {
            Console.WriteLine("\n\nAll tasks:\n\n");
            var allTasksJson = await client.GetStringAsync("https://localhost:44325/" + "api/task");
            var allTasks = JsonConvert.DeserializeObject<List<TaskDTO>>(allTasksJson);
            foreach (var item in allTasks)
            {
                Console.WriteLine("Id: " + item.Id + ", name: " + item.Name);
            }
        }

        private static async Task GetTaskById(HttpClient client)
        {
            Console.Write("\n\nWrite id: ");
            int taskId = Int32.Parse(Console.ReadLine());
            var taskJson = await client.GetStringAsync("https://localhost:44325/" + "api/task/" + taskId.ToString());
            var task = JsonConvert.DeserializeObject<TaskDTO>(taskJson);
            Console.WriteLine("Your task is " + task.Id.ToString() + ". " + task.Name);
        }

        private static async Task UpdateTask(HttpClient client)
        {
            Console.Write("\n\nWrite task id: ");
            int taskId = Int32.Parse(Console.ReadLine());
            Console.Write("\nWrite name: ");
            string taskName = Console.ReadLine();
            Console.Write("\nWrite description: ");
            string taskDescription = Console.ReadLine();
            Console.Write("\nWrite user id: ");
            var userId = Int32.Parse(Console.ReadLine());
            Console.Write("\nWrite project id: ");
            var projectId = Int32.Parse(Console.ReadLine());
            var task = new TaskDTO { Id = taskId, Name = taskName, Description = taskDescription, PerformerId = userId, ProjectId = projectId, State = TaskStates.Started };
            var taskJson = JsonConvert.SerializeObject(task);
            var taskRequest = new StringContent(taskJson, Encoding.UTF8, "application/json");
            var taskResponseJson = await client.PutAsync("https://localhost:44325/" + "api/task", taskRequest).Result.Content.ReadAsStringAsync();
            var taskResponse = JsonConvert.DeserializeObject<TaskDTO>(taskResponseJson);
            Console.WriteLine("Task was sucessfully updated! Task id: " + taskResponse.Id.ToString() + ". " + taskResponse.Name);
        }

        private static async Task DeleteTask(HttpClient client)
        {
            Console.Write("\n\nWrite id: ");
            int taskId = Int32.Parse(Console.ReadLine());
            await client.DeleteAsync("https://localhost:44325/" + "api/task/" + taskId.ToString());
            Console.WriteLine("Task with id: {0} was sucessfully deleted!", taskId);
        }

        private static async Task AddNewTask(HttpClient client)
        {
            Console.Write("\nWrite name: ");
            var taskName = Console.ReadLine();
            Console.Write("\nWtite description: ");
            var taskDescription = Console.ReadLine();
            Console.Write("\nWrite performer id: ");
            var performerId = Int32.Parse(Console.ReadLine());
            Console.Write("\nWrite project id: ");
            var projectId = Int32.Parse(Console.ReadLine());
            var taskJson = JsonConvert.SerializeObject(new TaskCreatedDTO { Name = taskName, Description = taskDescription, PerformerId = performerId, ProjectId = projectId });
            var task = new StringContent(taskJson, Encoding.UTF8, "application/json");
            var taskResponseJson = await client.PostAsync("https://localhost:44325/" + "api/task", task);
            var taskResponse = JsonConvert.DeserializeObject<TaskDTO>(taskResponseJson.Content.ReadAsStringAsync().Result);
            Console.WriteLine("Task with id {0} and name {1} was sucessfully posted!", taskResponse.Id.ToString(), taskResponse.Name);
        }

        private static async Task GetAllProjects(HttpClient client)
        {
            Console.WriteLine("\n\nAll projects:\n\n");
            var allProjectsJson = await client.GetStringAsync("https://localhost:44325/" + "api/project");
            var allProjects = JsonConvert.DeserializeObject<List<ProjectDTO>>(allProjectsJson);
            foreach (var item in allProjects)
            {
                Console.WriteLine("Id: " + item.Id + ", name: " + item.Name);
            }
        }

        private static async Task GetProjectById(HttpClient client)
        {
            Console.Write("\n\nWrite id: ");
            int projectId = Int32.Parse(Console.ReadLine());
            var projectJson = await client.GetAsync("https://localhost:44325/" + "api/project/" + projectId.ToString());
            var project = JsonConvert.DeserializeObject<ProjectDTO>(projectJson.Content.ReadAsStringAsync().Result);
            Console.WriteLine("Your project is " + project.Id.ToString() + ". " + project.Name);
        }

        private static async Task UpdateProject(HttpClient client)
        {
            Console.Write("\n\nWrite project id: ");
            int projectId = Int32.Parse(Console.ReadLine());
            Console.Write("\nWrite name: ");
            string projectName = Console.ReadLine();
            Console.Write("\nWrite description: ");
            string projectDescription = Console.ReadLine();
            Console.Write("\nWrite author id: ");
            var userId = Int32.Parse(Console.ReadLine());
            Console.Write("\nWrite team id: ");
            var teamId = Int32.Parse(Console.ReadLine());
            var project = new ProjectDTO { Id = projectId, Name = projectName, Description = projectDescription, AuthorId = userId, TeamId = teamId};
            var projectJson = JsonConvert.SerializeObject(project);
            var projectRequest = new StringContent(projectJson, Encoding.UTF8, "application/json");
            var projectResponseJson = await client.PutAsync("https://localhost:44325/" + "api/project", projectRequest);
            var projectResponse = JsonConvert.DeserializeObject<ProjectDTO>(projectResponseJson.Content.ReadAsStringAsync().Result);
            Console.WriteLine("Project was sucessfully updated! Project id: " + projectResponse.Id.ToString() + ". " + projectResponse.Name);
        }

        private static async Task DeleteProject(HttpClient client)
        {
            Console.Write("\n\nWrite id: ");
            int projectId = Int32.Parse(Console.ReadLine());
            var projectResponseJson = await client.DeleteAsync("https://localhost:44325/" + "api/project/" + projectId.ToString());
            Console.WriteLine("Project with id: {0} was sucessfully deleted!", projectId);
        }

        private static async Task AddNewProject(HttpClient client)
        {
            Console.Write("\nWrite name: ");
            var projectName = Console.ReadLine();
            Console.Write("\nWtite description: ");
            var projectDescription = Console.ReadLine();
            Console.Write("\nWrite author id: ");
            var authorId = Int32.Parse(Console.ReadLine());
            Console.Write("\nWrite deadline. Year: ");
            var year = Int32.Parse(Console.ReadLine());
            Console.Write(", month: ");
            var month = Int32.Parse(Console.ReadLine());
            Console.Write(", day: ");
            var day = Int32.Parse(Console.ReadLine());
            var deadLine = new DateTime(year, month, day);
            Console.Write("\nWrite team id: ");
            var teamId = Int32.Parse(Console.ReadLine());
            var projectJson = JsonConvert.SerializeObject(new ProjectCreatedDTO { Name = projectName, Description = projectDescription, AuthorId = authorId, Deadline = deadLine, TeamId = teamId });
            var project = new StringContent(projectJson, Encoding.UTF8, "application/json");
            var projectResponseJson = await client.PostAsync("https://localhost:44325/" + "api/project", project);
            var projectResponse = JsonConvert.DeserializeObject<ProjectDTO>(projectResponseJson.Content.ReadAsStringAsync().Result);
            Console.WriteLine("Project with id {0} and name {1} was sucessfully posted!", projectResponse.Id.ToString(), projectResponse.Name);
        }

        private static async Task GetAllTeams(HttpClient client)
        {
            Console.WriteLine("\n\nAll teams:\n\n");
            var allTeamsJson = await client.GetStringAsync("https://localhost:44325/" + "api/team");
            var allTeams = JsonConvert.DeserializeObject<List<TeamDTO>>(allTeamsJson);
            foreach (var item in allTeams)
            {
                Console.WriteLine("Id: " + item.Id + ", name: " + item.Name);
            }
        }

        private static async Task GetTeamById(HttpClient client)
        {
            Console.Write("\n\nWrite id: ");
            int teamId = Int32.Parse(Console.ReadLine());
            var teamJson = await client.GetStringAsync("https://localhost:44325/" + "api/team/" + teamId.ToString());
            var team = JsonConvert.DeserializeObject<TeamDTO>(teamJson);
            Console.WriteLine("Your team is " + team.Id.ToString() + ". " + team.Name);
        }

        private static async Task UpdateTeam(HttpClient client)
        {
            Console.Write("\n\nWrite team id: ");
            int teamId = Int32.Parse(Console.ReadLine());
            Console.Write("\nWrite name: ");
            string teamName = Console.ReadLine();
            var team = new TeamDTO { Id = teamId, Name = teamName};
            var teamJson = JsonConvert.SerializeObject(team);
            var teamRequest = new StringContent(teamJson, Encoding.UTF8, "application/json");
            var teamResponseJson = await client.PutAsync("https://localhost:44325/" + "api/team", teamRequest);
            var teamResponse = JsonConvert.DeserializeObject<TeamDTO>(teamResponseJson.Content.ReadAsStringAsync().Result);
            Console.WriteLine("Team was sucessfully updated! Team id: " + teamResponse.Id.ToString() + ". " + teamResponse.Name);
        }

        private static async Task DeleteTeam(HttpClient client)
        {
            Console.Write("\n\nWrite id: ");
            int teamId = Int32.Parse(Console.ReadLine());
            var teamResponseJson = await client.DeleteAsync("https://localhost:44325/" + "api/team/" + teamId.ToString());
            Console.WriteLine("Team with id: {0} was sucessfully deleted!", teamId);
        }

        private static async Task AddNewTeam(HttpClient client)
        {
            Console.Write("\nWrite name: ");
            var teamName = Console.ReadLine();
            var teamJson = JsonConvert.SerializeObject(new TeamCreatedDTO { Name = teamName });
            var team = new StringContent(teamJson, Encoding.UTF8, "application/json");
            var teamResponseJson = await client.PostAsync("https://localhost:44325/" + "api/team", team);
            var teamResponse = JsonConvert.DeserializeObject<TeamDTO>(teamResponseJson.Content.ReadAsStringAsync().Result);
            Console.WriteLine("Team with id {0} and name {1} was sucessfully posted!", teamResponse.Id.ToString(), teamResponse.Name);
        }

        private static async Task GetAllUsers(HttpClient client)
        {
            Console.WriteLine("\n\nAll users:\n\n");
            var allUsersJson = await client.GetStringAsync("https://localhost:44325/" + "api/user");
            var allUsers = JsonConvert.DeserializeObject<List<UserDTO>>(allUsersJson);
            foreach (var item in allUsers)
            {
                Console.WriteLine("Id: " + item.Id + ", name: " + item.FirstName + " " + item.LastName);
            }

        }

        private static async Task GetUserById(HttpClient client)
        {
            Console.Write("\n\nWrite id: ");
            int userId = Int32.Parse(Console.ReadLine());
            var userJson = await client.GetStringAsync("https://localhost:44325/" + "api/user/" + userId.ToString());
            var user = JsonConvert.DeserializeObject<UserDTO>(userJson);
            Console.WriteLine("Your user is " + user.Id.ToString() + ". " + user.FirstName + " " + user.LastName);
        }

        private static async Task UpdateUser(HttpClient client)
        {
            Console.Write("\n\nWrite user id: ");
            int userId = Int32.Parse(Console.ReadLine());
            Console.Write("\nWrite first name: ");
            string userFirstName = Console.ReadLine();
            Console.Write("\nWrite last name: ");
            string userLastName = Console.ReadLine();
            var user = new UserDTO { Id = userId, FirstName = userFirstName, LastName = userLastName };
            var userJson = JsonConvert.SerializeObject(user);
            var userRequest = new StringContent(userJson, Encoding.UTF8, "application/json");
            var userResponseJson = await client.PutAsync("https://localhost:44325/" + "api/user", userRequest);
            var userResponse = JsonConvert.DeserializeObject<UserDTO>(userResponseJson.Content.ReadAsStringAsync().Result);
            Console.WriteLine("User was sucessfully updated! User id: " + userResponse.Id.ToString() + ". " + userResponse.FirstName + " " + userResponse.LastName);
        }

        private static async Task DeleteUser(HttpClient client)
        {
            Console.Write("\n\nWrite id: ");
            int userId = Int32.Parse(Console.ReadLine());
            var userResponseJson = await client.DeleteAsync("https://localhost:44325/" + "api/user/" + userId.ToString());
            Console.WriteLine("User with id: {0} and was sucessfully deleted!", userId);
        }

        private static async Task AddNewUser(HttpClient client)
        {
            Console.Write("\nWrite first name: ");
            var userFirstName = Console.ReadLine();
            Console.Write("\nWrite last name: ");
            var userLastName = Console.ReadLine();
            Console.Write("\nWrite your birthday. Year: ");
            var year = Int32.Parse(Console.ReadLine());
            Console.Write(", month: ");
            var month = Int32.Parse(Console.ReadLine());
            Console.Write(", day: ");
            var day = Int32.Parse(Console.ReadLine());
            var birthday = new DateTime(year, month, day);
            Console.Write("\nWrite email: ");
            var email = Console.ReadLine();
            Console.Write("\nWrite team id: ");
            var teamId = Int32.Parse(Console.ReadLine());
            var userJson = JsonConvert.SerializeObject(new UserCreatedDTO { FirstName = userFirstName, LastName = userLastName, Birthday = birthday, Email = email, TeamId = teamId });
            var user = new StringContent(userJson, Encoding.UTF8, "application/json");
            var userResponseJson = await client.PostAsync("https://localhost:44325/" + "api/user", user);
            var userResponse = JsonConvert.DeserializeObject<UserDTO>(userResponseJson.Content.ReadAsStringAsync().Result);
            Console.WriteLine("User with id {0} and name {1} {2} was sucessfully posted!", userResponse.Id.ToString(), userResponse.FirstName, userResponse.LastName);
        }

        private static async Task AmountOfTasksOfProject(HttpClient client)
        {
            Console.WriteLine("\n\n\t1. Ammount of tasks of concrete user`s project.\n\n");
            Console.Write("Write id of user: ");
            int userId = Int32.Parse(Console.ReadLine());
            Console.WriteLine();
            var task1Json = await client.GetStringAsync("https://localhost:44325/api/linq/user/" + userId.ToString() + "/tasks/amount");
            var task = JsonConvert.DeserializeObject<List<ProjectDictionaryDTO>>(task1Json);
            foreach (var item in task)
            {
                Console.WriteLine("Project name: {0}, ammount of tasks: {1}", item.Key.Name, item.Value.ToString());
            }
        }

        private static async Task ConcreteUsersTask(HttpClient client)
        {
            Console.WriteLine("\n\n\t2. List of concrete user`s tasks with length < 45 symbols.\n\n");
            Console.Write("Write id of user: ");
            int userId = Int32.Parse(Console.ReadLine());
            Console.WriteLine();
            var taskJson = await client.GetStringAsync("https://localhost:44325/api/linq/user/" + userId.ToString() + "/tasks");
            var task = JsonConvert.DeserializeObject<List<TaskDTO>>(taskJson);
            foreach (var item in task)
            {
                Console.WriteLine("Task name: {0}({1}).", item.Name, item.Description);
            }
        }

        private static async Task TasksCompletedIn2021(HttpClient client)
        {
            Console.WriteLine("\n\n\t3. List of concrete user`s tasks that were completed in 2021.\n\n");
            Console.Write("Write id of user: ");
            int userId = Int32.Parse(Console.ReadLine());
            Console.WriteLine();
            var taskJson = await client.GetStringAsync("https://localhost:44325/api/linq/user/" + userId.ToString() + "/tasks/finished");
            var task = JsonConvert.DeserializeObject<List<TaskDictionaryDTO>>(taskJson);
            foreach (var item in task)
            {
                Console.WriteLine("Id: {0}, name: {1}", item.Key.ToString(), item.Value);
            }
        }

        private static async Task TeamsWith10MoreUser(HttpClient client)
        {
            Console.WriteLine("\n\n\t4. List of teams with 10 and more y.o. users, sorted by time of registration discending and grouped by teams.\n\n");
            var taskJson = await client.GetStringAsync("https://localhost:44325/api/linq/teams/older-then10");
            var task = JsonConvert.DeserializeObject<List<TeamWithUsersStruct>>(taskJson);
            foreach (var item in task)
            {
                Console.WriteLine("\tTeam id: {0}, team name: {1}, users: ", item.Id, item.TeamName);
                foreach (var user in item.Users)
                {
                    Console.Write("{0} {1},", user.LastName, user.FirstName);
                }
                Console.WriteLine("\n");
            }
            Console.WriteLine();

        }

        private static async Task SortedListOfUsers(HttpClient client)
        {
            Console.WriteLine("\n\n\t5. List of users sorted by first name wits tasks, sorted by length of name discending.\n\n");
            var taskJson = await client.GetStringAsync("https://localhost:44325/api/linq/users/sorted");
            var task = JsonConvert.DeserializeObject<List<UserDictionaryDTO>>(taskJson);
            foreach (var item in task)
            {
                Console.WriteLine("\tUser name: {0} {1}, tasks:", item.Key.FirstName, item.Key.LastName);
                foreach (var t in item.Value)
                {
                    Console.Write("{0}, ", t.Name);
                }
                Console.WriteLine("\n");
            }
            Console.WriteLine();

        }

        private static async Task GetUsersInfo(HttpClient client)
        {
            Console.WriteLine("\n\n\t6. Special struct#1.\n\n");
            Console.Write("Write id of user: ");
            int userId = Int32.Parse(Console.ReadLine());
            Console.WriteLine();
            var taskJson = await client.GetStringAsync("https://localhost:44325/api/linq/user/" + userId.ToString() + "/info");
            var task = JsonConvert.DeserializeObject<UserStructDTO>(taskJson);
            Console.WriteLine("UserId: {0}, user name: {1} {2}.", task.User.Id, task.User.FirstName, task.User.LastName);
            Console.WriteLine("Last user`s project: {0}({1}) created at: {2}", task.LastProject.Name, task.LastProject.Description, task.LastProject.CreatedAt.ToString());
            Console.WriteLine("Ammount of tasks of last user`s project: {0}.", task.AmmountOfTaskInLastProject.ToString());
            Console.WriteLine("Ammount of not finished or canceled tasks: {0}.", task.AmmoutOfNotFinishedOrClosedTasks.ToString());
            Console.WriteLine("The longest user`s task: {0}({1})", task.TheLongestTask.Name, task.TheLongestTask.Description);
            Console.WriteLine();

        }

        private static async Task GetProjectsInfo(HttpClient client)
        {
            Console.WriteLine("\n\n\t7. Special struct#2.\n\n");
            var taskJson = await client.GetStringAsync("https://localhost:44325/api/linq/projects/info");
            var task = JsonConvert.DeserializeObject<List<ProjectStruct>>(taskJson);
            foreach (var project in task)
            {
                Console.WriteLine("\tProject: {0}. {1}({2}).", project.Project.Id.ToString(), project.Project.Name, project.Project.Description);
                if (project.theLongestTask != null)
                {
                    Console.WriteLine("The longest task: {0}({1}).", project.theLongestTask.Name, project.theLongestTask.Description);
                    Console.WriteLine("The shortest task: {0}({1}).", project.theShortestTask.Name, project.theShortestTask.Description);
                }
                else
                {
                    Console.WriteLine("This project doesn`t contain any task");
                }
                if (project.ammountOfUsers != 0)
                {
                    Console.WriteLine("The ammount of users in project`s team: {0}", project.ammountOfUsers.ToString());
                }
                else
                {
                    Console.WriteLine("Description of the project <= 20 symbols and ammount of project`s task >= 3.");
                }
                Console.WriteLine("\n");
            }

        }
    }
}
